#include "ChessGame.h"
#include <iostream>

Piece* ChessGame::getPiece(Point position, PieceType type, PieceColor color)
{

	switch (type)
	{
	case PieceType::blank:
		return new Blank(position);
		break;
	case PieceType::king:
		return new King(position, color);
		break;
	case PieceType::queen:
		return new Queen(position, color);
		break;
	case PieceType::rook:
		return new Rook(position, color);
		break;
	case PieceType::bishop:
		return new Bishop(position, color);
		break;
	case PieceType::knight:
		return new Knight(position, color);
		break;
	case PieceType::pawn:
		return new Pawn(position, color);
		break;
	default:
		return nullptr;
		break;
	}
}

Point ChessGame::findKing(const PieceColor color)
{
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			if (this->board[i][j]->getColor() == color && this->board[i][j]->getType() == PieceType::king) {
				return this->board[i][j]->getPosition();
			}
		}
	}
}

bool ChessGame::checkCurrCheck(Point & to_move, Point & new_pos)
{
	bool flag = false;

	Piece* toSave = this->board[new_pos.getXI()][new_pos.getYI()];
	this->board[to_move.getXI()][to_move.getYI()]->move(new_pos, this->board);

	King toCheck(findKing(this->curr), this->curr);
	if (toCheck.isCheck(this->board)) flag = true;

	this->board[new_pos.getXI()][new_pos.getYI()]->move(to_move, this->board);
	this->board[new_pos.getXI()][new_pos.getYI()] = toSave;

	return flag;
}

PieceColor ChessGame::swap(PieceColor color)
{
	if (color == PieceColor::white) return PieceColor::black;
	return PieceColor::white;
}

void ChessGame::pawnToQueen(Point pos)
{
	if (board[pos.getXI()][pos.getYI()]->getType() == PieceType::pawn) {
		if (board[pos.getXI()][pos.getYI()]->getColor() == PieceColor::white && pos.getXI() == 0 || 
			board[pos.getXI()][pos.getYI()]->getColor() == PieceColor::black && pos.getXI() == 7) {
			PieceColor temp = board[pos.getXI()][pos.getYI()]->getColor();
			delete board[pos.getXI()][pos.getYI()];
			board[pos.getXI()][pos.getYI()] = new Queen(pos, temp);
		}
	}
}

ChessGame::ChessGame() : BoardGame(NAME, BOARD_SIZE)
{
	this->curr = PieceColor::white;
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			PieceColor col = PieceColor::transparent;
			if (i <= 1) col = PieceColor::black;
			if (i >= 6) col = PieceColor::white;

			this->board[i][j] = getPiece(Point(i, j), this->locations[i][j], col);
		}
	}
}

ChessGame::~ChessGame()
{
}

std::string ChessGame::start()
{
	if (this->started) return std::string();

	return this->START_POS;
}

int ChessGame::nextMove(std::string move_id)
{
	std::string piece_to_move = move_id.substr(0, 2);
	std::string end_point = move_id.substr(2, 2);

	Point to_move(piece_to_move);
	Point point(end_point);

	std::cout << "to move: x: " << to_move.getX() << " y: " << to_move.getY() << std::endl;
	std::cout << "point: x: " << point.getX() << " y: " << point.getY() << std::endl;
	std::cout << "to move: xi: " << to_move.getXI() << " yi: " << to_move.getYI() << std::endl;
	std::cout << "point: xi: " << point.getXI() << " yi: " << point.getYI() << std::endl;

	if (this->board[to_move.getXI()][to_move.getYI()]->getColor() != this->curr) 
		return MoveCodes::error_no_piece;


	int move_code = this->board[to_move.getXI()][to_move.getYI()]->checkMove(point, this->board);

	if (move_code == MoveCodes::ok) {
		if (checkCurrCheck(to_move, point)) return MoveCodes::error_chess;

		this->board[to_move.getXI()][to_move.getYI()]->move(point, this->board);
		
		pawnToQueen(point);

		King toCheck(findKing(swap(this->curr)), swap(this->curr));
		//if (toCheck.isCheckmate(this->board)) move_code = MoveCodes::ok_checkmate;
		if (toCheck.isCheck(this->board)) move_code = MoveCodes::ok_chess;
		this->curr = swap(this->curr);
	}
	return move_code;
}

void ChessGame::printBoard()
{
	std::cout << std::endl;
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			switch (this->board[i][j]->getType()) {
			case PieceType::blank:
				std::cout << "_";
				break;
			case PieceType::king:
				std::cout << "k";
				break;
			case PieceType::queen:
				std::cout << "q";
				break;
			case PieceType::knight:
				std::cout << "n";
				break;
			case PieceType::pawn:
				std::cout << "p";
				break;
			case PieceType::rook:
				std::cout << "r";
				break;
			case PieceType::bishop:
				std::cout << "b";
				break;
			default:
				std::cout << "*";
				break;
			}
			// TODO: remove cout
			//std::cout << "(" << board[i][j]->getPosition().getX() << ", " << board[i][j]->getPosition().getY() << ")";
			//std::cout << "(" << board[i][j]->getPosition().getXI() << ", " << board[i][j]->getPosition().getYI() << ")";
			switch (board[i][j]->getColor())
			{
			case PieceColor::white:
				std::cout << "W";
				break;
			case PieceColor::black:
				std::cout << "B";
				break;
			case PieceColor::transparent:
				std::cout << "T";
				break;
			default:
				break;
			}
			std::cout << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}


