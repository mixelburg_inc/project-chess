#pragma once
#include <iostream>
#include <string>

class Point;

class Point {
public:
	Point();
	Point(const std::string pos);
	Point(const int x, const int y);
	Point(const Point& other);
	~Point();

	bool isValidPos(int pos);

	/**
	 * @brief converts char to int 
	 * given char is a letter position on a chess board 
	 * (a = 1, b = 2, ...)
	 * @param letter - given char to convert
	 * @return converted char
	*/
	int letterToInt(char letter);

	/**
	 * @brief converts char number to int number
	 * @param - num given char number to convert
	 * @return converted char
	*/
	int charNumToInt(char num);

	// basic getters and setters
	int getX() const;
	int getY() const;

	// return coordinates as index (coord - 1)
	int getXI() const;
	int getYI() const;

	void setX(const int val);
	void setY(const int val);
	void setXY(const std::string pos); // pos[0] is letter, pos[1] is char number

	friend bool operator==(const Point& ths, const Point& other);
	Point& operator=(const Point& other);
private:
	int x;
	int y;
};
