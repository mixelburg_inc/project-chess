#include "BoardGame.h"

BoardGame::BoardGame(const std::string n, const int b_size = 8) : name(n), board_size(b_size) {
	this->board = new Piece**[board_size];
	for (int i = 0; i < board_size; i++)
	{
		board[i] = new Piece*[board_size];
	}
}

BoardGame::~BoardGame()
{
	for (int i = 0; i < board_size; i++)
	{
		for (int j = 0; j < board_size; j++)
		{
			delete this->board[i][j];
		}
		delete[] this->board[i];
	}
	delete[] this->board;
	this->board = nullptr;
}

