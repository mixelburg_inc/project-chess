#pragma once

#include "BoardGame.h"
#include "Point.h"
#include "Piece.h"
#include "King.h"
#include "Bishop.h"
#include "Knight.h"
#include "Queen.h"
#include "Rook.h"

class ChessGame;

class ChessGame : public BoardGame {
private:
	PieceColor curr = PieceColor::white;
	static const int BOARD_SIZE = 8;
	const std::string NAME = "chess";
	const std::string START_POS = "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0";

	PieceType locations[BOARD_SIZE][BOARD_SIZE] = {
		// row 1
		{PieceType::rook, PieceType::knight, PieceType::bishop, PieceType::king,
		PieceType::queen, PieceType::bishop, PieceType::knight, PieceType::rook},

		// row 2
		{PieceType::pawn, PieceType::pawn, PieceType::pawn, PieceType::pawn,
		PieceType::pawn, PieceType::pawn, PieceType::pawn, PieceType::pawn},

		// rows 3 - 6
		{PieceType::blank, PieceType::blank, PieceType::blank, PieceType::blank,
		PieceType::blank, PieceType::blank, PieceType::blank, PieceType::blank},
		{PieceType::blank, PieceType::blank, PieceType::blank, PieceType::blank,
		PieceType::blank, PieceType::blank, PieceType::blank, PieceType::blank},
		{PieceType::blank, PieceType::blank, PieceType::blank, PieceType::blank,
		PieceType::blank, PieceType::blank, PieceType::blank, PieceType::blank},
		{PieceType::blank, PieceType::blank, PieceType::blank, PieceType::blank,
		PieceType::blank, PieceType::blank, PieceType::blank, PieceType::blank},

		// row 7
		{PieceType::pawn, PieceType::pawn, PieceType::pawn, PieceType::pawn,
		PieceType::pawn, PieceType::pawn, PieceType::pawn, PieceType::pawn},

		// row 8
		{PieceType::rook, PieceType::knight, PieceType::bishop, PieceType::king,
		PieceType::queen, PieceType::bishop, PieceType::knight, PieceType::rook},
	};


	Piece* getPiece(Point position, PieceType type = PieceType::blank, PieceColor color = PieceColor::transparent);
	Point findKing(const PieceColor color);
	bool checkCurrCheck(Point & to_move, Point & new_pos);
	PieceColor swap(PieceColor color);
	void pawnToQueen(Point pos);
public:
	ChessGame();
	~ChessGame();

	std::string start();
	int nextMove(std::string move_id);
	void printBoard();
};

