#pragma once

#include"Pawn.h"

class Pawn;
class King;

class King : public Piece {
public:
	King(const Point& pos, const PieceColor col);
	virtual std::vector<Point> movePoints(Piece *** board) override;

	/**
	 * @brief returns std::vector containing all of the points to which
	 * enemy player can move his/her pieces
	 * @param board game board
	 * @return std::vector with move points
	*/
	std::vector<Point> killPoints(Piece *** board);
	
	/**
	 * @brief checks if it is checkmate to this King
	 * @param board game board
	 * @return true or false correspondingly
	*/
	bool isCheck(Piece *** board);

	/**
	 * @brief checks if it is check to this King
	 * @param board game board
	 * @return true or false correspondingly
	*/
	bool isCheckmate(Piece *** board);
};
