#include"Piece.h"


class Pawn :public Piece {
private:
	bool firstMovment;
public:
	Pawn(const Point& pos, const PieceColor col);
	/* returns a vector that contains all the posible points a player can move this piece to
	   board - the game board
	*/
	virtual std::vector<Point> movePoints(Piece*** board) override;
	bool isFirstMov() const;
	std::vector<Point> murderPoints(Piece*** board);
};
