#include"Rook.h"


Rook::Rook(const Point& pos, const PieceColor col) : Piece(pos, PieceType::rook, col) {
}

std::vector<Point> Rook::movePoints(Piece *** board) {
	std::vector<Point> points;
	int delta[][2]{
		{1,0},
		{-1,0},
		{0,1},
		{0,-1}
	};
	//for (int i = 0; i < 8; ++i) {
	//	for (int j = 0; j < 8; j++)
	//	{
	//		if (board[i][j]->getColor() != this->color) {
	//			if (board[i][j]->getPosition().getX() == this->getPosition().getX() && board[i][j]->getPosition().getY() != this->getPosition().getY()) {
	//				points.push_back(Point(board[i][j]->getPosition().getXI(), board[i][j]->getPosition().getYI()));
	//			}
	//			else if (board[i][j]->getPosition().getY() == this->getPosition().getY() && board[i][j]->getPosition().getX() != this->getPosition().getX()) {
	//				points.push_back(Point(board[i][j]->getPosition().getXI(), board[i][j]->getPosition().getYI()));
	//			}
	//		}
	//	}
	//}
	for (int* i : delta) {
		Point curr(this->position);
		do //x - down, y - up
		{
			try {
				curr = Point(curr.getXI() + i[0], curr.getYI() + i[1]);
			}
			catch (int e) {
				break;
			}
			points.push_back(curr);
		} while (board[curr.getXI()][curr.getYI()]->getColor() == PieceColor::transparent);
	}
	return points;
}