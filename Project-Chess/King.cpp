#include "King.h"

King::King(const Point& pos, const PieceColor col) : Piece(pos, PieceType::king, col)
{
}

std::vector<Point> King::movePoints(Piece *** board)
{
    std::vector<Point> points;

    int values[][2] = {
        { 1,  1},
        {-1, +1},
        {+1, -1},
        {-1, -1},
        { 0,  1},
        { 0, -1},
        { 1,  0},
        {-1,  0},
    };

    for (int* pair : values) {
        try {
            points.push_back(Point(this->position.getXI() + pair[0], this->position.getYI() + pair[1]));
        }
        catch (int e) {}
    }
    for (int i = 0; i < points.size(); i++)
    {
        if (isFriend(points[i], board)) {
            points.erase(points.begin() + i);

        }

    }


    return points;
}

std::vector<Point> King::killPoints(Piece *** board)
{
    std::vector<Point> kill_points;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (board[i][j]->getColor() != this->color) {
                std::vector<Point> pts;
                if (board[i][j]->getType() == PieceType::pawn) {
                    pts = Pawn(board[i][j]->getPosition(), board[i][j]->getColor()).murderPoints(board);
                }
                else {
                    pts = board[i][j]->movePoints(board);
                }
                for (Point p : pts) {
                    kill_points.push_back(p);
                }
            }
        }
    }
    return kill_points;
}

bool King::isCheck(Piece *** board)
{
    return containsPoint(killPoints(board), this->position);
}

bool King::isCheckmate(Piece *** board)
{
    std::vector<Point> move_points = movePoints(board);
    std::vector<Point> kill_points = killPoints(board);

    for (Point i : move_points) {
   }
    bool flag = true;
    for (Point point : move_points) {
        if (!(containsPoint(kill_points, point))) return false;
    }
    return isCheck(board) && flag;
}
