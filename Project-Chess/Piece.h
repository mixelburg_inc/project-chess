#pragma once

#include <vector>
#include "Point.h"


enum class PieceType {
	blank,
	king,
	queen,
	rook,
	bishop,
	knight,
	pawn,
};

enum class PieceColor {
	transparent,
	white,
	black,
};

enum MoveCodes {
	// ok codes
	ok = 0,
	ok_chess = 1,
	ok_checkmate = 8,

	// error codes
	error_piece_at_dest = 3,
	error_chess = 4,
	error_move = 6,
	error_same_pos = 7,
	error_no_piece = 2,
};


class Piece {
protected:
	Point position;
	PieceType type;
	PieceColor color;

	/**
	 * @brief Checks if given vector contains given Point
	 * @param points std::vector of points
	 * @param point Point object
	 * @return true if contains, othervise - false
	*/
	bool containsPoint(const std::vector<Point> points, const Point & point);
public:
	Piece(const Point& pos, const PieceType t, const PieceColor col);
 	virtual ~Piece();

	/**
	 * @brief has to return std::vector containing all points
	 * to which this Piece can move
	 * @param board game board
	 * @return std::vector<Point> with moving points
	*/
	virtual std::vector<Point> movePoints(Piece *** board) = 0;

	/**
	 * @brief validates move to a given point
	 * @param newPos Point object representing new position
	 * @param board game board
	 * @return number representing move code
	*/
	int checkMove(Point& newPos, Piece *** board);

	/**
	 * @brief moves this Piece to a given point
	 * @param newPos Point object representing new position
	 * @param board game board
	*/
	void move(Point& newPos, Piece *** board);

	/**
	 * @brief checks is Piece at a given position is a friend (same color)
	 * @param pos 
	 * @param board 
	 * @return true if it is friend othervise - false
	*/
	bool isFriend(Point& pos, Piece *** board) const;

	PieceColor getColor() const;
	PieceType getType() const;
	void setPosition(const Point & newPosition);
	Point getPosition() const;

	Piece& operator=(const Piece& other);
};

class Blank : public Piece {
	virtual std::vector<Point> movePoints(Piece *** board) override { return std::vector<Point>(); };
public:
	Blank(Point& position) : Piece(position, PieceType::blank, PieceColor::transparent) {};
	Blank() : Piece(Point(1, 1), PieceType::blank, PieceColor::transparent) {};
};


